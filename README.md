
1. Clone the repository.
2. Install dependencies using the following command: `pip install -r requirements.txt.`
3. Create a `.env` file in the project root directory and add your Telegram Bot token as `BOT_TOKEN` in this file.
4. Create a `users.db` file in the project root directory and create a table users with columns `id`, `chat_id`, and `username`.
