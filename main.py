import sqlite3
import time
from datetime import datetime
import telebot
import os
from dotenv import load_dotenv

load_dotenv()

bot = telebot.TeleBot(os.getenv("BOT_TOKEN"))


class TeleBot:

    def get_users(self):
        with sqlite3.connect('users.db') as con:
            cursor = con.cursor()

            cursor.execute("SELECT * FROM users LIMIT 1000;")
            return cursor.fetchall()

    def generate_html_report(self, data):
        html = "<html><body>"
        html += "<h3>Report</h3>"
        html += "<table>"
        for key, value in data.items():
            html += f"<tr><td>{key}</td><td>{value}</td></tr>"
        html += "</table>"
        html += "</body></html>"
        report_time = datetime.now().strftime("%Y-%m-%d")
        with open(f"report({report_time}).html", "w") as f:
            f.write(html)

    def send_messages_to_users(self):
        users = self.get_users()
        report_data = {}
        send_limit = 0
        for user in users:
            chat_id = user[1]
            username = user[2]
            message = "test message"
            try:
                bot.send_message(chat_id, message)
                send_limit += 1
                report_data[username] = 'active'
            except telebot.apihelper.ApiTelegramException:
                report_data[username] = 'inactive'

            if send_limit == 30:
                time.sleep(60)
                send_limit = 0
        self.generate_html_report(report_data)


if __name__ in "__main__":
    foo = TeleBot()
    foo.send_messages_to_users()
